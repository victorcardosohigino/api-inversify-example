# API Example Inversify

<p align="center">
  <a href="https://www.linkedin.com/in/victor-cardoso-higino/" target="_blank" rel="noopener noreferrer">
    <img alt="Made by" src="https://img.shields.io/badge/made%20by-Victor%20Cardoso%20Higino-blue">
  </a>
</p>

<p align="center">
  <a href="#%EF%B8%8F-sobre-o-projeto">Sobre o projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-tecnologias-utilizadas">Tecnologias utilizadas</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-como-rodar-a-api">Como rodar a API</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#%EF%B8%8F-como-rodar-os-testes">Como rodar os testes</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-documentação-e-uso-da-api">Documentação e uso da API</a>
</p>

## 💉 Sobre o projeto

Essa API foi criada para possibilitar o cadastro de usuários utilizando injeção de dependência com inversify.

## 💻 Tecnologias utilizadas

Tecnologias utilizadas para desenvolver esta API.

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [Express](https://expressjs.com/pt-br/)
- [TypeORM](https://typeorm.io/#/)
- [Iversify](https://inversify.io/)
- [uuid v4](https://github.com/thenativeweb/uuidv4/)
- [PostgreSQL](https://www.postgresql.org/)
- [Jest](https://jestjs.io/)
- [SuperTest](https://github.com/visionmedia/supertest)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [EditorConfig](https://editorconfig.org/)
- [Swagger](https://swagger.io/)
- [Git](https://git-scm.com/)
- [Gitlab](https://gitlab.com/)
- [Docker](https://www.docker.com/)

## 🚀 Como rodar a API

### Requisitos para rodar a API

- [Docker](https://www.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/)

> Abra o terminal e execute os seguintes comandos

**Clonar o projeto e acessar a pasta do projeto**

```bash
$ git clone https://gitlab.com/victorcardosohigino/api-inversify-example.git

$ cd api-inversify-example
```

**Copiar arquivo com as variáveis de ambiente**

```bash
$ cp .env.example .env
```

**Executar Docker-compose com o Node.js e o Postgres**

```bash
$ docker-compose up --build -d
```

**Executar as Migrations para criação do banco de dados**

```bash
$ docker exec example-api npm run typeorm migration:run
```

**API funcionando**

Para ver a API funcionando acesse http://localhost:3333

## ✔️ Como rodar os testes

**Com a API funcionando, execute o comando abaixo para rodar os testes unitários e testes de integração**

```bash
$ docker exec example-api npm run test
```

## ⚙️ Como ver os logs da API no docker

**Execute o comando abaixo**

```bash
$ docker-compose logs
```
> Alternativamente, você pode usar a opção `-f`, assim: `docker-compose logs -f` para ficar acompanhando os logs gerados pela API.
> Se quiser parar de acompanhar os logs, no terminal aperte `CTRL+C`.

## 🛑 Como parar a API

**Execute o comando abaixo**

```bash
$ docker-compose stop
```
> Para rodar a API novamente, basta executar o comando: `docker-compose up -d`

## 📝 Documentação e uso da API

Para usar e testar os endpoints da API na sua máquina acesse a documentação localhost: [Documentação Localhost](http://localhost:3333/api-docs)

**Conexão com o banco de dados**

Para conectar ao banco de dados local através de uma IDE externa, com a API rodando, você pode utilizar as seguintes configurações:<br>
Host: localhost<br>
Database: example<br>
Username: example<br>
Password: 123456

---

Feito por Victor Cardoso Higino 👋 &nbsp;[Ver Linkedin](https://www.linkedin.com/in/victor-cardoso-higino/)
