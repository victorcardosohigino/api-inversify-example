import { Request, Response } from "express";

import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { container } from "@shared/container";

class CreateUserController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, email, password } = request.body;

    const createUserUseCase = container.get(CreateUserUseCase);

    await createUserUseCase.execute({
      name,
      email,
      password,
    });

    return response.status(201).send();
  }
}

export { CreateUserController };
