import { Container } from "inversify";

import { UsersRepository } from "@modules/users/infra/typeorm/repositories/UsersRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";

import TYPES from "../types";

const container = new Container();

container.bind(CreateUserUseCase).toSelf().inSingletonScope();
container
  .bind<IUsersRepository>(TYPES.IUsersRepository)
  .to(UsersRepository)
  .inSingletonScope();

export { container };
