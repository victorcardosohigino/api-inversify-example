import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { CreateUserController } from "@modules/users/useCases/createUser/CreateUserController";

const usersRoutes = Router();

const createUserController = new CreateUserController();

usersRoutes.post(
  "/",
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      email: Joi.string().email().required().max(250),
      password: Joi.string().required().min(6),
    },
  }),
  createUserController.handle
);

export { usersRoutes };
